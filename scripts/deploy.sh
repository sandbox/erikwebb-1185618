#!/bin/bash

BASE_DIR=/var/www/vhosts/self2011
case $ENVIRONMENT in
'@self2011.prod')
   TARGET_DIR=current;;
'@self2011.stg')
   TARGET_DIR=staging;;
*)
   exit 1;;
esac

rsync -Ca ./ vps:$BASE_DIR/$GIT_TAG

ssh vps "cd $BASE_DIR ; git checkout $GIT_TAG ; rm -f $TARGET_DIR ; ln -fs $GIT_TAG $TARGET_DIR"

