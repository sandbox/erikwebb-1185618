require "test/unit"
require "rubygems"
gem "selenium-client"
require "selenium/client"

class SiteLogoTest < Test::Unit::TestCase

  def setup
    @verification_errors = []
    @selenium = Selenium::Client::Driver.new \
      :host => "ondemand.saucelabs.com",
      :port => 80,
      :browser => "{\"username\": \"erikwebb\",\"access-key\":\"c4daeb0f-2158-447b-afd0-d9bc359622ac\",\"browser\": \"iehta\",\"browser-version\":\"9\",\"job-name\":\"Site Logo Test\",\"max-duration\":1800,\"record-video\":true,\"user-extensions-url\":\"\",\"os\":\"Windows 2008\"}",
      :url => "http://self2011test.erikwebb.net/",
      :timeout_in_second => 60

    @selenium.start_new_browser_session
  end
  
  def teardown
    @selenium.close_current_browser_session
    assert_equal [], @verification_errors
  end
  
  def test_site_logo
    @selenium.open "/"
    @selenium.wait_for_page_to_load "60000"
    assert @selenium.is_element_present("xpath=//a[@id='logo']/img")
  end
end
