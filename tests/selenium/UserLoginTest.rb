require "test/unit"
require "rubygems"
gem "selenium-client"
require "selenium/client"

class UserLoginTest < Test::Unit::TestCase

  def setup
    @verification_errors = []
    @selenium = Selenium::Client::Driver.new \
      :host => "ondemand.saucelabs.com",
      :port => 80,
      :browser => "{\"username\": \"erikwebb\",\"access-key\":\"c4daeb0f-2158-447b-afd0-d9bc359622ac\",\"browser\": \"safariproxy\",\"browser-version\":\"5\",\"job-name\":\"User Login Test\",\"max-duration\":1800,\"record-video\":true,\"user-extensions-url\":\"\",\"os\":\"Windows 2003\"}",
      :url => "http://self2011test.erikwebb.net/",
      :timeout_in_second => 60

    @selenium.start_new_browser_session
  end
  
  def teardown
    @selenium.close_current_browser_session
    assert_equal [], @verification_errors
  end
  
  def test_user_login
    @selenium.open "/"
    @selenium.wait_for_page_to_load "60000"
    @selenium.type "id=edit-name", "test"
    @selenium.type "id=edit-pass", "test"
    @selenium.click "id=edit-submit"
    @selenium.wait_for_page_to_load "60000"
    @selenium.click "xpath=//a[.='My account']"
  end
end
